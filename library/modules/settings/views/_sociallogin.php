<?php
/**
 * Created by PhpStorm.
 * User: Ayham
 * Date: 11/15/2019
 * Time: 8:38 AM
 */
use usni\library\utils\Html;
use usni\UsniAdaptor;

$facebookLoginLabel       = Html::tag('legend', UsniAdaptor::t('settings', 'Facebook Login'));
$model  = $formDTO->getModel();
?>

<?= $facebookLoginLabel;?>
<?= $form->field($model, 'isFacebookLoginEnabled', ['horizontalCssClasses' => ['wrapper'   => 'col-sm-12'],
    'horizontalCheckboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}"])->checkbox();?>
<?= $form->field($model, 'facebookClientId')->textInput(); ?>
<?= $form->field($model, 'facebookClientSecret')->textInput(); ?>
<hr/>

